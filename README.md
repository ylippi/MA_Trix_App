
[![SWH](https://archive.softwareheritage.org/badge/swh:1:dir:a78cb8f1c0fca114d6fc7fcdcf6659545570da1b/)](https://archive.softwareheritage.org/swh:1:dir:a78cb8f1c0fca114d6fc7fcdcf6659545570da1b;origin=https://forgemia.inra.fr/ylippi/MA_Trix_App;visit=swh:1:snp:7b967cd54f717398d98856b2eea8daa5dce7f25a;anchor=swh:1:rev:9f11a68e8c6a0de9148425ee69a0e49c159181c9)
[![SWH](https://archive.softwareheritage.org/badge/origin/https://forgemia.inra.fr/ylippi/MA_Trix_App/)](https://archive.softwareheritage.org/browse/origin/?origin_url=https://forgemia.inra.fr/ylippi/MA_Trix_App)

# MATRiX is a shiny application for Mining and functional Analysis of TRanscriptomics data.

## Contents

- [Introduction](#introduction)
- [Installation](#installation)
- [Contact](#contact)
- [Acknowledgements](#acknowledgements)


## Introduction

MATRiX is a shiny application dedicated to **M**ining and **A**nalysis of **Tr**anscriptom**ics** data using common biostatistic result files (statistical test result table, normalised data table and samples-groups information). This application helps biologists to perform data exploration with PCA, Venn diagrams, StripCharts, volcanoplots, and Heatmap clustering of genes lists (selecting statistical thresholds) and functional enrichment analyses using a connection to [enrichr](https://maayanlab.cloud/Enrichr/).

MATRiX app is working with specific data produced by the limma, DESeq2, edgeR packages, resulting p-values are adjusted according to the Benjamini and Hochberg procedure [Benjamini and Hochberg 1995].
PCA is computed with the FactoMineR package and the plot is produced with the factoextra package, for the Heatmap and Venn diagram the graphs are obtained respectively with the gplots and [jvenn plugin](http://jvenn.toulouse.inra.fr/app/index.html).
This application works only with specific data, you can check the example file (MA_Trix_App/downloadData.zip)

Here's the global workflow passing by the experiment to the visualization:

![](./www/whatmaen.png)

and also a [video Presentation](https://www.youtube.com/watch?v=lfI0zRYzeJs)

## Installation

MATRiX is available for R>3.5.0. The installation, download and execution can all be performed with a small R script :
First you'll need to install RJava in the aim to using RDAVIDWebService.
You can install it using the following commands:
```
sudo apt-get install default-jdk
sudo R CMD javareconf #to associate it with R
sudo apt-get install r-cran-rjava
sudo apt-get install libgdal1-dev libproj-dev
sudo apt-get install libv8-3.14-dev
```
```
## Install RDAVIDWebService into R
source("https://bioconductor.org/biocLite.R")
biocLite("RDAVIDWebService")
install.packages("rJava")

## Load RDAVIDWebService
library(RDAVIDWebService)

## Load shiny packages
if(!require('shiny')){
 install.packages('shiny')
 library(shiny)
}

# Install dependencies, download last version of MATRiX from github and run matrix in one command :
runGitHub('GeT-TRiX/MA_Trix_App')
```
If RDAVIDWebService and Shiny are installed on your machine can also run the app as following :
```
git clone https://github.com/GeT-TRiX/MA_Trix_App
chmod +x ./cmd.sh
./cmd.sh
```

## Contact

Here are our mail [Franck Soubès] (franck.soubes@inra.fr) or [Yannick Lippi] (yannick.lippi@inra.fr) for any questions, suggestions or if you need help to use MATRiX, dont hesitate to contact us.

## Acknowledgements

The main contributors to MATRiX:

Yannick Lippi, (Initiator, beta-testing, feature suggestions),

Franck Soubès, (Coding, Unit testing, documentation, packaging, feature suggestions),

Didier Laborie, (Virtual machine and DNS),

TOXALIM Team: BioToMyc & TIM, (beta-testing, feature suggestions)  

Claire Naylies, (suggestions)

